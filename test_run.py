from utils import excel_info
from app_utils import get_scene,post_process_video,check_file,video_download
import os,glob,pandas as pd
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
from moviepy.editor import VideoFileClip,concatenate_videoclips,AudioFileClip,CompositeAudioClip

path = "creds/"
path_1 = "input/"
path_2 = "process/"
path_3 = "output/"
path_4 = "bg/"

sheet_id = "1VtXLaI8jnBlTO1ptOyrVIwBEGqNq3bG7oTU5QFwPyj0"

#Query Excel Data
data,scenes = excel_info(path,sheet_id)

api_key = "komunidad_qc"

#Get the data based on API Key
data_1 = (data[data["api_key"].isin([api_key])]).reset_index(drop=True)

def checker_video(data_1,path_1):

    #Check if the API Key has uploaded file
    os.chdir(path_1)
        
    filename = pd.DataFrame(glob.glob("*mp4"))
    
    if len(filename) == 0:
        
        filename_0 = filename
        
    else:
    
        filename_0 = (filename[filename[0].str.contains(data_1["location"].iloc[0])]).reset_index(drop=True)
    
    os.chdir("../")
    
    return(filename_0)
    
filename_0 = checker_video(data_1,path_1)

if len(filename_0) == 0:
    
    print("No File Uploaded!")

else:
    
    print("File existing!")
    
    #Get the scene based on inputted API Key
    data_2 = (scenes[scenes["location"].isin([data_1["location"].iloc[0]])]).reset_index(drop=True)
    
    scene_list = list(data_2["scene"].drop_duplicates())
    scene_filter = ["Intro","Radar Rainfall","Rain Forecast","Extro"]
    
    loc_0 = data_1["location"].iloc[0]
    
    process = glob.glob(path_2+"*.mp4")
    if len(process) == 0:
        pass
        print("No Files to be deleted!")
    else:
        for a in process:
            if loc_0 in a:
                os.remove(a)
                print("Removed Process Files!")
    
    output = glob.glob(path_3+"*.mp4")
    if len(output) == 0:
        pass
        print("No Files to be deleted!")
    else:    
        for f_1 in output:
            if loc_0 in f_1:
                os.remove(f_1)
                print("Removed Output!")
                
    data_3 = (data_2[data_2["scene"].isin(scene_filter)]).reset_index(drop=True)
    
    for i,v in enumerate(data_3["location"]):
        #i = 0
        #v = data_3["location"].iloc[i]
        
        start_time = data_3["start_time"].iloc[i]
        end_time = data_3["end_time"].iloc[i]
        loc = data_3["location"].iloc[0]
        scene = data_3["scene"].iloc[i]
        arrangement = data_3["arrangement"].iloc[i]
        
        os.chdir(path_1)
        
        file_name = pd.DataFrame(glob.glob(loc+".mp4"),columns=["file"])
        
        if len(file_name) == 0:
            
            print("File Name is Invalid!")
        
        else:
            pass
        
        os.chdir("../")
        
        ffmpeg_extract_subclip(path_1+file_name["file"][0],start_time,end_time,targetname=path_2+str(arrangement)+"_"+scene+"_"+loc+".mp4")
    
    loc = data_1["location"].iloc[0]
    
    file_name_1 = pd.DataFrame(glob.glob(path_2+"*"),columns=["file"])
    file_name_2 = (file_name_1[file_name_1["file"].str.contains(loc)]).reset_index(drop=True)
    
    file_name_4 = []
    for i_1,v_1 in enumerate(file_name_2["file"]):
        #i_1 = 0
        #v_1 = file_name_2["file"].iloc[i_1]
        
        file_name_3 = VideoFileClip(v_1)
        
        file_name_4.append(file_name_3)
        
    result_clip = concatenate_videoclips(file_name_4) 
    result_clip = result_clip.set_audio(CompositeAudioClip([AudioFileClip(path_4+loc+".mp3").set_duration(result_clip.duration)]))
    result_clip.to_videofile(path_3+loc+".mp4",remove_temp=True,codec="libx264")
    
    for i_2,v_2 in enumerate(file_name_4):
        #i_2 = 0
        #v_2 = file_name_4[i_2]
        
        v_2.close()
    #result_clip.reader.close()