import pandas as pd 
from google.oauth2 import service_account
import streamlit as st

#Add the caching minutes
cache_min = 5

#Get the sheet creds
def sheet_creds(path):
    import pickle
    from googleapiclient.discovery import build
    
    #Read the token.pickle
    token = open(path+"token.pickle", "rb")
    creds = pickle.load(token)
    service = build('sheets', 'v4', credentials = creds)
    
    #Access the google sheets api
    sheet = service.spreadsheets()    
    print ("GSheets creds accepted!")
    return (sheet)

#Add the caching
@st.cache(suppress_st_warning=True,ttl = cache_min * 60,allow_output_mutation=True)
#Read the alert input on google sheets
def excel_info(path,sheet_id):
    #Get the sheet creds
    sheet = sheet_creds(path)
    
    #Insert sheet_id and range source
    range_source = ["location!A1:AA","scenes!A1:AA"]
    sheet_name = ["location","scenes"]
    
    data_3 = []
    #Read the data    
    for i,v in enumerate(range_source):
        #i = 0 
        #v = range_source[i]
        #Parse sheet data
        data_type = sheet_name[i]
        
        #Access the gsheets
        data = sheet.values().get(spreadsheetId = sheet_id
                           ,range = v).execute()
        
        #Convert data to dataframe
        data_1 = (pd.DataFrame(data.get("values", [])))
        
        #Make the first column the header 
        col = data_1.iloc[0]
        data_2 = (data_1.iloc[1:]).reset_index(drop = True)
        data_2.columns = col
        
        #Change lat and lon to float
        if data_type == "scenes":
            data_2["start_time"] = data_2["start_time"].astype(float)
            data_2["end_time"] = data_2["end_time"].astype(float)
        else: 
            pass
    
        data_3.append(data_2)
    
    return(data_3)