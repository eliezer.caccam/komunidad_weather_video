import os,glob,re
import pandas as pd
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
import time
import streamlit as st

def check_file(path_1,data_1,video_file):
    #Check if the API Key has uploaded file
    os.chdir(path_1)
        
    filename = pd.DataFrame(glob.glob("*mp4"))
    
    os.chdir("../")
    
    if len(filename) == 0:
        
        filename_0 = filename
        
        if video_file is not None:
                 
            #Download the Uploaded Video
            video = video_download(data_1,path_1,video_file)
        
            filename_0 = (video[video["name"].str.contains(data_1["location"].iloc[0])]).reset_index(drop=True)
        
    else:
    
        filename_0 = (filename[filename[0].str.contains(data_1["location"].iloc[0])]).reset_index(drop=True)
        
        if video_file is not None:
                 
            #Download the Uploaded Video
            video_download(data_1,path_1,video_file)
    
    return(filename_0)
def video_download(data_1,path_1,video_file):
    # TO See details
    file_details = {"filename":video_file.name, "filetype":video_file.type,
                      "filesize":video_file.size}
#            st.write(file_details)
    
    loc_0 = data_1["location"].iloc[0]

    process = glob.glob(os.path.join(path_1,"*.mp4"))
    for f_0 in process:
        if loc_0 in f_0:
            os.remove(f_0)

    #Saving upload
    with open(os.path.join(path_1,video_file.name),"wb") as f:
        f.write((video_file).getbuffer())
        
    os.chdir(path_1)
    
    video = pd.DataFrame(glob.glob("*.mp4"),columns = ["name"])
    
    os.chdir("../")
        
    return(video)


def get_scene(api_key,data_2,data_1,scene_filter,path_1,path_2,path_3):
    
    loc_0 = data_1["location"].iloc[0]
    
    import glob,os
    
    process = glob.glob(os.path.join(path_2,"*.mp4"))
    for a in process:
        if loc_0 in a:
            os.remove(a)
    
    output = glob.glob(os.path.join(path_3,"*.mp4"))
    for f_1 in output:
        if loc_0 in f_1:
            os.remove(f_1)
    
    #Debug
    #scene_filter = ["Intro","Radar Rainfall","Earthquake Report"]
    data_3 = (data_2[data_2["scene"].isin(scene_filter)]).reset_index(drop=True)
    
    for i,v in enumerate(data_3["location"]):
        #i = 0
        #v = data_3["location"].iloc[i]
        
        start_time = data_3["start_time"].iloc[i]
        end_time = data_3["end_time"].iloc[i]
        loc = data_3["location"].iloc[0]
        scene = data_3["scene"].iloc[i]
        arrangement = data_3["arrangement"].iloc[i]
        
        os.chdir(path_1)
        
        file_name = pd.DataFrame(glob.glob(loc+".mp4"),columns=["file"])
        
        if len(file_name) == 0:
            msg = "File name is invalid!"
            st.warning(msg)
        else:
            pass
            
        os.chdir("../")
        
        ffmpeg_extract_subclip(path_1+file_name["file"][0],start_time,end_time,targetname=path_2+str(arrangement)+"_"+scene+"_"+loc+".mp4")    

    return

def post_process_video(data_1,path_2,path_3,path_4):
    
    from moviepy.editor import VideoFileClip,concatenate_videoclips,AudioFileClip,CompositeAudioClip
    
    loc = data_1["location"].iloc[0]

    os.chdir(path_2)
    
    file_name = pd.DataFrame(glob.glob("*"),columns=["file"])
    file_name_1 = (file_name[file_name["file"].str.contains(loc)]).reset_index(drop=True)
    
    file_name_3 = []
    for i,v in enumerate(file_name_1["file"]):
        #i = 0
        #v = file_name_1["file"].iloc[i]
        
        file_name_2 = VideoFileClip(v)
        file_name_3.append(file_name_2)
    
    #file_name_1["file"] = file_name_3
    
    os.chdir("../")
    
    #files = list(file_name_1["file"].drop_duplicates())
    
    result_clip = concatenate_videoclips(file_name_3)
    audio_file_1 = CompositeAudioClip([AudioFileClip(path_4+loc+".mp3").set_duration(result_clip.duration)])
    result_clip = result_clip.set_audio(audio_file_1)
    #result_clip.write_videofile(path_3+loc+".mp4")
    result_clip.to_videofile(path_3+loc+".mp4",remove_temp=True)
    
    for i_2,v_2 in enumerate(file_name_3):
        #i_2 = 0
        #v_2 = file_name_4[i_2]
        
        v_2.close()
    
    return