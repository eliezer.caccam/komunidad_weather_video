from utils import excel_info
from app_utils import get_scene,post_process_video,check_file,video_download
import streamlit as st
import os,glob,pandas as pd

def checker_video(path_1,video_file):
    os.chdir(path_1)
        
    filename = pd.DataFrame(glob.glob("*mp4"),columns=["name"])
    
    os.chdir("../")
    
    if len(filename) == 0:

        if video_file is not None:
            
            # TO See details
            file_details = {"filename":video_file.name, "filetype":video_file.type,
                              "filesize":video_file.size}
            #st.write(file_details)
            
            loc_0 = data_1["location"].iloc[0]
        
            process = glob.glob(os.path.join(path_1,"*.mp4"))
            for f_0 in process:
                if loc_0 in f_0:
                    os.remove(f_0)
            
            #Saving upload
            with open(os.path.join(path_1,video_file.name),"wb") as f:
                f.write((video_file).getbuffer())
            
            filename_0 = (filename[filename["name"].str.contains(data_1["location"].iloc[0])]).reset_index(drop=True)
        
            msg = "Uploaded File Completed!"
            st.success(msg)
        
        else:
            
            msg = "No File in the Directory!"
            st.warning(msg)
            
            col = ["name"]
            filename_0 = pd.DataFrame(columns=col)
            
    elif len(filename) != 0:
        
        filename_0 = (filename[filename["name"].str.contains(data_1["location"].iloc[0])]).reset_index(drop=True)
        
        msg = "Existing File Detected! Upload new file!"
        st.warning(msg)
        
        if video_file is not None:
            
            # TO See details
            file_details = {"filename":video_file.name, "filetype":video_file.type,
                              "filesize":video_file.size}
            #st.write(file_details)
            
            loc_0 = data_1["location"].iloc[0]
        
            process = glob.glob(os.path.join(path_1,"*.mp4"))
            for f_0 in process:
                if loc_0 in f_0:
                    os.remove(f_0)
            
            #Saving upload
            with open(os.path.join(path_1,video_file.name),"wb") as f:
                f.write((video_file).getbuffer())
            
            filename_0 = (filename[filename["name"].str.contains(data_1["location"].iloc[0])]).reset_index(drop=True)
        
            msg = "Re-Uploaded File Completed!"
            st.success(msg)
        
    return(filename_0)

path = "creds/"
path_1 = "input/"
path_2 = "process/"
path_3 = "output/"
path_4 = "bg/"

sheet_id = "1VtXLaI8jnBlTO1ptOyrVIwBEGqNq3bG7oTU5QFwPyj0"

#Query Excel Data
data,scenes = excel_info(path,sheet_id)

#api_key = "komunidad_qc"

#Create text input for API
api_key = st.sidebar.text_input("API Key",type="password")

if st.sidebar.checkbox("Login") == True:
    
    #Get the data based on API Key
    data_1 = (data[data["api_key"].isin([api_key])]).reset_index(drop=True)
    
    video_file = st.sidebar.file_uploader("Upload Weather Video",type="mp4")
    
    filename_0 = checker_video(path_1,video_file)
       
    #try:
    if len(filename_0) != 0:
        
        #Get the data based on API Key
        data_1 = (data[data["api_key"].isin([api_key])]).reset_index(drop=True)
           
        #Get the location based on data
        data_2 = (scenes[scenes["location"].isin([data["location"].iloc[0]])]).reset_index(drop=True)
        
        #Create Filter
        scene_list = list(data_2["scene"].drop_duplicates())
        #scene_filter = ["Intro","Radar Rainfall","Extro"]
        scene_filter = st.sidebar.multiselect("Choose Scene",scene_list,default=["Intro","Radar Rainfall","Rain Forecast","Temperature Forecast","Wind Forecast","Extro"])
        
        if st.sidebar.button("Process Video") == True:
                 
            msg = "Processing Video... Please Wait!"
            st.warning(msg)
 
            #Crop the needed scenes
            #get_scene(api_key,data_2,data_1,scene_filter,path_1,path_2,path_3)
            loc_0 = data_1["location"].iloc[0]
    
            #import glob,os
            from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
            
            process = glob.glob(os.path.join(path_2,"*.mp4"))
            for f in process:
                if loc_0 in f:
                    os.remove(f)
            
            output = glob.glob(os.path.join(path_3,"*.mp4"))
            for f_1 in output:
                if loc_0 in f_1:
                    os.remove(f_1)
            
            #Debug
            #scene_filter = ["Intro","Radar Rainfall","Earthquake Report"]
            data_3 = (data_2[data_2["scene"].isin(scene_filter)]).reset_index(drop=True)
            
            for i,v in enumerate(data_3["location"]):
                #i = 0
                #v = data_3["location"].iloc[i]
                
                start_time = data_3["start_time"].iloc[i]
                end_time = data_3["end_time"].iloc[i]
                loc = data_3["location"].iloc[0]
                scene = data_3["scene"].iloc[i]
                
                os.chdir(path_1)
                
                file_name = pd.DataFrame(glob.glob(loc+".mp4"),columns=["file"])
                
                if len(file_name) == 0:
                    msg = "File name is invalid!"
                    st.warning(msg)
                else:
                    pass
                    
                os.chdir("../")
                
                ffmpeg_extract_subclip(path_1+file_name["file"][0],start_time,end_time,targetname=path_2+str(i)+"_"+scene+"_"+loc+".mp4")    
            
            #Process the video (Combine all scenes)
            post_process_video(data,path_2,path_3,path_4)
                

#             msg_1 = "Success!"
#             st.success(msg_1)
#        
                
