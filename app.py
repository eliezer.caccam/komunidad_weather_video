from utils import excel_info
from app_utils import get_scene,post_process_video,check_file,video_download
import streamlit as st

path = "creds/"
path_1 = "input/"
path_2 = "process/"
path_3 = "output/"
path_4 = "bg/"

sheet_id = "1VtXLaI8jnBlTO1ptOyrVIwBEGqNq3bG7oTU5QFwPyj0"

#Set the page
st.set_page_config(initial_sidebar_state = "auto",layout = "wide",page_title = "Weather Video Processor")

#Query Excel Data
data,scenes = excel_info(path,sheet_id)

api_key = "komunidad_qc"

#Create text input for API
api_key = st.sidebar.text_input("API Key",type="password")

if st.sidebar.checkbox("Login") == True:
    
    #Get the data based on API Key
    data_1 = (data[data["api_key"].isin([api_key])]).reset_index(drop=True)

    if len(data_1) == 0:
    
        msg = "Invalid Credentials! Please Try Again!"
        st.sidebar.warning(msg)
    
    else:
        
        video_file = st.sidebar.file_uploader("Upload Weather Video",type="mp4")
        
        #Check if the API Key has uploaded file
        filename_0 = check_file(path_1,data_1,video_file)
        
        if len(filename_0) == 0:
            
            msg = "No Uploaded File!"
            st.sidebar.warning(msg)
        
        else:
            
             #Get the data based on API Key
             data_1 = (data[data["api_key"].isin([api_key])]).reset_index(drop=True)
               
             #Get the location based on data
             data_2 = (scenes[scenes["location"].isin([data["location"].iloc[0]])]).reset_index(drop=True)
            
             #Create Filter
             scene_list = list(data_2["scene"].drop_duplicates())
             #scene_filter = ["Intro","Radar Rainfall","Extro"]
             scene_filter = st.sidebar.multiselect("Choose Scene",scene_list,default=["Intro","Radar Rainfall","Rain Forecast","Temperature Forecast","Wind Forecast","Extro"])
             
             try:
                 if st.sidebar.button("Process Video") == True:
                     
                     msg = "Processing Video... Please Wait!"
                     st.warning(msg)
     
                     #Crop the needed scenes
                     get_scene(api_key,data_2,data_1,scene_filter,path_1,path_2,path_3)
    
                     #Process the video (Combine all scenes)
                     post_process_video(data,path_2,path_3,path_4)
    
                     msg_1 = "Success!"
                     st.success(msg_1)
                     
             except PermissionError:
                
                msg = "Error Occurred! Restart the Console! Ctrl + C to Shut Down"
                st.warning(msg)
            
        if st.sidebar.checkbox("Display Video") == True:
            
            if len(filename_0) == 0:
                msg = "No Video File to Process!"
                st.warning(msg)
            
            else:
                st.video(path_3+data_1["location"].iloc[0]+".mp4",format="video/mp4")

st.sidebar.write("Note: This system is filename-sensitive. Make sure that the name format is correct all the times!")