mkdir -p ~/.streamlit/
echo "[theme]
primaryColor = '#0060ad'
backgroundColor = '#FFFFFF'
secondaryBackgroundColor = '#F0F2F6'
textColor = '#262730'
font = 'sans serif'
[server]
headless = true
port = $PORT
enableCORS = false
maxUploadSize = 2000
" > ~/.streamlit/config.toml
